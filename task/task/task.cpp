﻿#pragma once

#include <chrono>
#include <omp.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "Curves.h"

using namespace std;

const double pi = acos(-1);

using PObject = std::shared_ptr<Object>;
using PCircle = std::shared_ptr<Circle>;


vector<PObject> create() {
    int count_circles = 0, count_Ellipse = 0, count_Helix = 0;
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dist(1, 3);
    uniform_real_distribution<> dist2(-100, 100);

    vector<PObject> objects;
    for (int i = 0; i < 20 or count_circles < 1 or count_Ellipse < 1 or count_Helix < 1; ++i) {
        switch (dist(gen)) {
        case 1:
            objects.emplace_back(make_shared<Circle>(dist2(gen)));
            count_circles++;
            break;
        case 2:
            objects.emplace_back(make_shared<Ellipse>(dist2(gen), dist2(gen)));
            count_Ellipse++;
            break;
        default:
            objects.emplace_back(make_shared<Helix>(dist2(gen), dist2(gen)));
            count_Helix++;
            break;
        }
    }
    return objects;
}

void print_objects(const vector<PObject>& objects, double t = pi / 4) {
    for (auto& ps : objects)
    {
        const type_info& ti{ typeid(*ps) };
        cout << ti.name() << ' '
            << ps->get_point(pi / 4) << ' '
            << ps->get_derivative(pi / 4) << endl;
    }
}

vector<PCircle> get_circles(const vector<PObject>& objects) {
    vector<PCircle> circles;

    for (auto& ps : objects)
    {
        const type_info& ti{ typeid(*ps) };

        if (ti.hash_code() == typeid(Circle).hash_code())
        {
            circles.emplace_back(dynamic_pointer_cast<Circle>(ps));
        }
    }
    return circles;
}

void sort_circles(vector<PCircle>& circles) {
    sort(circles.begin(), circles.end(), [](const auto& cir1, const auto& cir2) {
        return (*cir1) < (*cir2);
        });
}

double summ_circles(vector<PCircle>& circles) {
    double sum = 0;
#pragma omp parallel for num_threads(8) reduction(+:sum)
    for (int i = 0; i < circles.size(); i++) {
        sum += circles[i]->get_R();
    }

    return sum;
}

int main()
{
    vector<PObject> objects = create();
    print_objects(objects);
    vector<PCircle> circles = get_circles(objects);

    for (auto& ps : circles) {
        cout << *ps << endl;
    }
    cout << endl << endl;
    sort_circles(circles);
    for (auto& ps : circles) {
        cout << *ps << endl;
    }
    double summ = summ_circles(circles);
    cout << summ << endl;
}

