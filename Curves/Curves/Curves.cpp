#include "pch.h"
#include <cmath>
#include <iostream>
#include "curves.h"

using PCircle = std::shared_ptr<Circle>;

std::ostream& operator<<(std::ostream& os, const vector3& p)
{
	return os << "{ " << p.x << ", " << p.y << ", " << p.z << " }";
}

bool operator<(const Circle& cir1, const Circle& cir2)
{
	return cir1.get_R() < cir2.get_R();
}

std::ostream& operator<<(std::ostream& os, const Circle& cir)
{
	return os << "R = " << cir.get_R();
}

Circle::Circle(double r)
{
	R = r >= 0 ? r : abs(r);
}

vector3 Circle::get_point(double t) const {
	return vector3(R * cos(t), R * sin(t), 0);
}

vector3 Circle::get_derivative(double t) const {
	return vector3(-R * sin(t), R * cos(t), 0);
}

bool Circle::operator<(const Circle& cir)const
{
	return R < cir.R;
}

bool Circle::operator<=(const Circle& cir)const
{
	return R <= cir.R;
}


Ellipse::Ellipse(double r, double r2)
{
	R = r >= 0 ? r : abs(r);
	R2 = r2 >= 0 ? r2 : abs(r2);
}

vector3 Ellipse::get_point(double t)const
{
	return vector3(R * cos(t), R2 * sin(t), 0);
}

vector3 Ellipse::get_derivative(double t)const
{
	return vector3(-R * sin(t), R2 * cos(t), 0);
}

Helix::Helix(double r, double s)
{
	R = r >= 0 ? r : abs(r);
	step = s;
}

vector3 Helix::get_point(double t)const
{
	return vector3(R * cos(t), R * sin(t), step * t);
}

vector3 Helix::get_derivative(double t)const
{
	return vector3(-R * sin(t), R * cos(t), step);
}
