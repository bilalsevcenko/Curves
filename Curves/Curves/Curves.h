#pragma once

#define MATHLIBRARY_API __declspec(dllexport)

class MATHLIBRARY_API vector3 {
public:
	double x, y, z;
	vector3() : x(0), y(0), z(0) {};
	vector3(double X, double Y, double Z) : x(X), y(Y), z(Z) {};
};

class MATHLIBRARY_API Object {
protected:
	double R;
public:
	Object() { R = 1; }
	virtual vector3 get_point(double t) const = 0;
	virtual vector3 get_derivative(double t) const = 0;

	virtual ~Object() {};
};

class MATHLIBRARY_API Circle : public Object {
public:
	explicit Circle(double r);
	vector3 get_point(double t)const;
	vector3 get_derivative(double t)const;
	bool operator <(const Circle& cir)const;
	bool operator <=(const Circle& cir)const;
	double get_R() const { return R; };
};

class MATHLIBRARY_API Ellipse : public Object {
	double R2;
public:
	Ellipse(double r, double r2);
	vector3 get_point(double t)const;
	vector3 get_derivative(double t)const;
};

class MATHLIBRARY_API Helix : public Object {
	double step;
public:
	Helix(double r, double s);
	vector3 get_point(double t)const;
	vector3 get_derivative(double t)const;
};

MATHLIBRARY_API std::ostream & operator<<(std::ostream & os, const vector3 & p);

MATHLIBRARY_API std::ostream& operator<<(std::ostream& os, const Circle& cir);

